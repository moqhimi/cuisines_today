package de.quandoo.recruitment.registry;

import org.junit.Test;

import de.quandoo.recruitment.registry.ds.DefaultStore;
import de.quandoo.recruitment.registry.ds.MapKey;
import de.quandoo.recruitment.registry.ds.MapValue;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.util.*;

public class StoreTest {
	
	@Test
	public void defaultStoreTest() {
		DefaultStore<Cuisine, Customer> ds = new DefaultStore<Cuisine, Customer>();
		
		Set<Customer> cuisine1CustomersList = new HashSet<Customer>();
		Set<Customer> cuisine2CustomersList = new HashSet<>();
		
		for(int i=1; i<10; i++) {
			cuisine1CustomersList.add(new Customer("customer"+i));
		}
		for(int i=1; i<20; i++) {
			cuisine2CustomersList.add(new Customer("customer"+i));
		}
		
		for(Customer c : cuisine1CustomersList) {
			ds.add(new Cuisine("cuisine1"), c);
		}
		
		for(Customer c : cuisine2CustomersList) {
			ds.add(new Cuisine("cuisine2"), c);
		}
		
		Set<Cuisine> allCuisines = new HashSet<>(Arrays.asList(new Cuisine("cuisine1"), new Cuisine("cuisine2")));
		
		Set<Customer> cuisine1Customers = ds.get(new Cuisine("cuisine1"));
		assertEquals(cuisine1Customers, cuisine1CustomersList);
		
		Set<Customer> cuisine2Customers = ds.get(new Cuisine("cuisine2"));
		assertEquals(cuisine2Customers, cuisine2CustomersList);
		
		Set<Cuisine> customer1Cuisines = ds.reversedGet(new Customer("customer1"));
		assertEquals(customer1Cuisines, allCuisines);
		
		Set<Cuisine> customer9Cuisines = ds.reversedGet(new Customer("customer9"));
		assertEquals(customer9Cuisines, allCuisines);
		
		Set<Cuisine> customer16Cuisines = ds.reversedGet(new Customer("customer16"));
		assertEquals(customer16Cuisines, new HashSet<>(Arrays.asList(new Cuisine("cuisine2"))));
		
	}
	
}
