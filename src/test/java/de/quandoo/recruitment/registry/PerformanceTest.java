package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.ds.DefaultStore;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;


import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;
import static org.junit.Assert.assertThat;

import java.util.Random;

public class PerformanceTest {
    private InMemoryCuisinesRegistry cuisinesRegistry;

    @Before
    public void init() {
        cuisinesRegistry = new InMemoryCuisinesRegistry();
    }


    @Test
    @Ignore
    public void performanceTest() {
        List<Cuisine> cuisines = InMemoryCuisinesRegistryTest.createCuisine();
        List<Customer> customers = createFunctionalTestCase(10000, cuisines, cuisinesRegistry);
        double timeInsert10K = extractInsertTime(customers, cuisines);
        double timeNeededTop10K = extractTopTime();
        double timeFetchTime1K =  extractFetchTime(cuisines);






        customers = createFunctionalTestCase(1000000, cuisines, cuisinesRegistry);
        double timeInsert1M= extractInsertTime(customers, cuisines);
        double timeNeededTop1M = extractTopTime();
        double timeFetchTime1M =  extractFetchTime(cuisines);

        assertThat(timeInsert1M/timeInsert10K, Matchers.lessThan(100.0));

        assertThat(timeNeededTop1M/timeNeededTop10K, Matchers.lessThan(2.0));

        assertThat(timeFetchTime1M/timeFetchTime1K, Matchers.lessThan(20.0));


    }

    private double extractTopTime() {
        long start = System.nanoTime();
        cuisinesRegistry.topCuisines(2);
        long end = System.nanoTime();
        return end - start;
    }

    private double extractInsertTime(List<Customer> customers, List<Cuisine> cuisines){
        long totalInsertTime = 0;
        int count=0;
        for (Customer customer : customers) {
            int random = new Random().nextInt(3);
            for (Cuisine cuisine : cuisines.subList(0, random + 1)) {
                long start1K = System.currentTimeMillis();
                cuisinesRegistry.register(customer, cuisine);
                long end1K = System.currentTimeMillis();
                totalInsertTime+=end1K-start1K;
                count++;
            }
        }
        return (totalInsertTime);
    }


    public double extractFetchTime(List<Cuisine> cuisines){

        double startTime=System.nanoTime();
        cuisinesRegistry.cuisineCustomers(cuisines.get(1));
        double endTime=System.nanoTime();
        return endTime-startTime;
    }



    public static List<Customer> createFunctionalTestCase(int number, List<Cuisine> cuisines, InMemoryCuisinesRegistry cuisinesRegistry) {
        DefaultStore store=(DefaultStore)cuisinesRegistry.getStore();
        store.clear();
        List<Customer> customers = InMemoryCuisinesRegistryTest.createCustomers("customer", number);
        return customers;

    }

}
