package de.quandoo.recruitment.registry;


import de.quandoo.recruitment.registry.ds.DefaultStore;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.hamcrest.core.IsEqual.equalTo;

import static org.hamcrest.core.Is.is;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertThat;

public class InMemoryCuisinesRegistryTest {


    private InMemoryCuisinesRegistry cuisinesRegistry;
    private static Logger logger= LogManager.getLogger(InMemoryCuisinesRegistryTest.class);
    @Before
    public void init() {
        cuisinesRegistry = new InMemoryCuisinesRegistry();
    }




    @Test
    public void testRegister2() {
        List<Cuisine> cuisines= createCuisine();
        List<Customer> customers = createCustomers("cust", 100);
        List<Customer> tmpCustomers = new ArrayList<>();
        int i = 0;
        for (Cuisine cuisine : cuisines) {
            customers.forEach(customer -> {
                if (!tmpCustomers.contains(customer)) {
                    cuisinesRegistry.register(customer, cuisine);
                }

            });
            tmpCustomers.add(customers.get(i));
            i++;
        }
        i=0;
        for (Cuisine cuisine : cuisines) {
            assertThat("size of customers which liked each cuisine", cuisinesRegistry.cuisineCustomers(cuisine).size(), is((customers.size() - i)));
            i++;
        }
        assertThat("top cuisine", cuisines.get(0), is(equalTo(cuisinesRegistry.topCuisines(1).get(0))));


    }


    @Test
    public void testRegister() {
        List<Cuisine> cuisines = createCuisine();
        List<Customer> customers = createCustomers("customer-", 20);
        customers.forEach(customer -> {
            cuisines.forEach(cuisine -> {
                cuisinesRegistry.register(customer, cuisine);
            });
        });
        cuisines.forEach(cuisine -> {
            assertThat("size of customers which liked each cuisine", cuisinesRegistry.cuisineCustomers(cuisine).size(), is(equalTo(customers.size())));
        });
        customers.forEach(customer -> {
            assertThat("size of cuisines liked by each customer", cuisinesRegistry.customerCuisines(customer).size(), is(equalTo(cuisines.size())));
        });
    }


    public static  List<Customer> createCustomers(String iden, int number) {
        List<Customer> customers = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            customers.add(Customer.CustomerBuilder.build(iden + i));
        }
        return customers;
    }


    public static List<Cuisine> createCuisine() {
        List<Cuisine> cuisines = new ArrayList<>();
        List<String> names = Arrays.asList("italian", "french", "german");
        names.forEach(name-> cuisines.add(new Cuisine(name)));
        return cuisines;

    }

    @Test
    public void integratedAccuracyTest() {

        InMemoryCuisinesRegistry registry = new InMemoryCuisinesRegistry(new DefaultStore<Cuisine, Customer>());

        List<Customer> customersOfItalianList = new ArrayList<Customer>();
        List<Customer> customersOfFrenchList = new ArrayList<Customer>();
        List<Customer> customersOfGermanList = new ArrayList<Customer>();

        for (int i = 1; i < 10; i++) {
            customersOfItalianList.add(new Customer("customer" + i));
        }
        for (int i = 1; i < 20; i++) {
            customersOfFrenchList.add(new Customer("customer" + i));
        }
        customersOfGermanList.add(new Customer("customer1"));

        for (Customer c : customersOfItalianList) {
            registry.register(c, new Cuisine("italian"));
        }

        for (Customer c : customersOfFrenchList) {
            registry.register(c, new Cuisine("french"));
        }

        for (Customer c : customersOfGermanList) {
            registry.register(c, new Cuisine("german"));
        }

        registry.register(new Customer("customer1"), new Cuisine("invalid"));

        List<Cuisine> allCuisines = Arrays.asList(new Cuisine("italian"), new Cuisine("french")
                , new Cuisine("german"));

        List<Customer> customersOfInvalidCuisine = registry.cuisineCustomers(new Cuisine("invalid"));
        assertEquals(customersOfInvalidCuisine, null);
        logger.info("customersOfInvalidCuisine = " + customersOfInvalidCuisine);

        List<Customer> customersOfItalian = registry.cuisineCustomers(new Cuisine("italian"));

        assertThat("List equality without order",
                customersOfItalianList, containsInAnyOrder(customersOfItalian.toArray()));

        logger.info("customersOfItalian = " + customersOfItalian);

        List<Customer> customersOfFrench = registry.cuisineCustomers(new Cuisine("french"));
        assertThat("List equality without order",
                customersOfFrenchList, containsInAnyOrder(customersOfFrench.toArray()));
        logger.info("customersOfFrench = " + customersOfFrench);

        List<Customer> customersOfGerman = registry.cuisineCustomers(new Cuisine("german"));
        assertThat("List equality without order",
                customersOfGermanList, containsInAnyOrder(customersOfGerman.toArray()));
        logger.info("customersOfGerman = " + customersOfGerman);

        List<Cuisine> customer1Cuisines = registry.customerCuisines(new Customer("customer1"));assertThat("List equality without order",
                allCuisines, containsInAnyOrder(customer1Cuisines.toArray()));
        logger.info("customer1Cuisines = " + customer1Cuisines);

        List<Cuisine> customer9Cuisines = registry.customerCuisines(new Customer("customer9"));
        assertThat("List equality without order",
                Arrays.asList(new Cuisine("italian"), new Cuisine("french")), containsInAnyOrder(customer9Cuisines.toArray()));
        logger.info("customer9Cuisines = " + customer9Cuisines);

        List<Cuisine> customer16Cuisines = registry.customerCuisines(new Customer("customer16"));
        assertThat("List equality without order",
                Arrays.asList(new Cuisine("french")), containsInAnyOrder(customer16Cuisines.toArray()));
        logger.info("customer16Cuisines = " + customer16Cuisines);

        List<Cuisine> top1 = registry.topCuisines(1);
        assertEquals(top1, Arrays.asList(new Cuisine("french")));
        logger.info("top1 = " + top1);

        List<Cuisine> top2 = registry.topCuisines(2);
        assertEquals(top2, Arrays.asList(new Cuisine("french"), new Cuisine("italian")));
        logger.info("top2 = " + top2);

        List<Cuisine> top3 = registry.topCuisines(3);
        assertEquals(top3, Arrays.asList(new Cuisine("french"), new Cuisine("italian"), new Cuisine("german")));
        logger.info("top3 = " + top3);

        List<Cuisine> top4 = registry.topCuisines(4);
        assertEquals(top4, Arrays.asList(new Cuisine("french"), new Cuisine("italian"), new Cuisine("german")));
        logger.info("top4 = " + top4);

        List<Cuisine> top100 = registry.topCuisines(100);
        assertEquals(top100, Arrays.asList(new Cuisine("french"), new Cuisine("italian"), new Cuisine("german")));
        logger.info("top100 = " + top100);


    }




}