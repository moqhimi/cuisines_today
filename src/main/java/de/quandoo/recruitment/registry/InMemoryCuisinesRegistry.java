package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.ds.DefaultStore;
import de.quandoo.recruitment.registry.ds.Store;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {
	
	private final static List<String> VALID_CUISINES = Arrays.asList("italian", "french", "german");
	private static Logger logger= LogManager.getLogger(InMemoryCuisinesRegistry.class);
	/**
	 * memory-based store to maintain objects and their relationships
	 */
	private final Store<Cuisine, Customer> store;

	/**
	 * to use DefaultStore if Store is not provided
	 */
	public InMemoryCuisinesRegistry() {
		this(new DefaultStore<Cuisine, Customer>());
	}
	
	public InMemoryCuisinesRegistry(Store<Cuisine, Customer> store) {
		this.store = store;
	}
	
	/**
	 * first validate input values and then put the couple into the map
	 */
    @Override
    public void register(final Customer customer, final Cuisine cuisine) 
    {
    	String err = this.validateCustomer(customer);
    	if(err==null) {
    		err = this.validateCuisine(cuisine);
    		if(err==null) {
        		this.store.add(cuisine, customer);
    		}
        	else {
        		logger.error(err);
        	}
    	}
    	else {
    		logger.error(err);
    	}
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
    	if(this.validateCuisine(cuisine) != null) {
    		return null;
    	}
        return this.store.get(cuisine).stream().collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
    	if(this.validateCustomer(customer) != null) {
    		return null;
    	}
    	return this.store.reversedGet(customer).stream().collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        return this.store.topKeysFromCache(n);
    }
    
    private String validateCustomer(Customer customer) {
    	if(customer==null || customer.getUuid()==null || customer.getUuid().length()==0) {
    		return "Invalid customer with null or empty uuid";
    	}
    	return null;
    }
    
    private String validateCuisine(Cuisine cuisine) {
    	if(cuisine==null || cuisine.getName()==null || cuisine.getName().length()==0) {
    		return "Invalid cuisine with null or empty name";
    	}
    	if(!VALID_CUISINES.contains(cuisine.getName())) {
    		return "Unknown cuisine : "+cuisine.getName()+
    				", please reach johny@bookthattable.de to update the code";
    	}
    	return null;
    }

    public Store getStore(){
    	return store;
	}
}
