package de.quandoo.recruitment.registry.model;

import de.quandoo.recruitment.registry.ds.MapKey;
import de.quandoo.recruitment.registry.ds.MapValue;

/**
 * represents the Cuisine class which is the key in the store.
 */
public class Cuisine implements MapKey{

    private final String name;
    
    /**
     * prevent to call empty constructor
     */
    private Cuisine(){
    	this.name = null;
    }
    
    public Cuisine(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

	@Override
	public String toString() {
		return "Cuisine [name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cuisine other = (Cuisine) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
