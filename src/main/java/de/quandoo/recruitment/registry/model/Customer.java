package de.quandoo.recruitment.registry.model;

import de.quandoo.recruitment.registry.ds.MapValue;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * represents the Customer class which is the value in the store.
 */


public class Customer implements MapValue {

    private String uuid;


    /**
     * prevent to call empty constructor
     */
    private Customer() {
        this.uuid = null;
    }

    public Customer(final String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    @Override
    public String toString() {
        return "Customer [uuid=" + uuid + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Customer other = (Customer) obj;
        if (uuid == null) {
            if (other.uuid != null)
                return false;
        } else if (!uuid.equals(other.uuid))
            return false;
        return true;
    }


    public static class CustomerBuilder {
        static Map<String, Customer> customerMap = new ConcurrentHashMap<>();

        public static Customer build(String uuid) {
            Customer customer = customerMap.get(uuid);
            if (customer == null) {
                customer = new Customer(uuid);
                customerMap.putIfAbsent(uuid, customer);

            }
            return customer;
        }
    }


}
