package de.quandoo.recruitment.registry.ds;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


/**
 * represents the default data structure which is in local mode and uses in-memory map
 * to maintain objects.
 */
public class DefaultStore<T1 extends MapKey, T2 extends MapValue> extends Store<T1, T2> {

    protected Map<T1, Set<T2>> directMap;
    protected Map<T2, Set<T1>> reversedMap;





    public DefaultStore() {
        this.directMap = new HashMap<T1, Set<T2>>();
        this.reversedMap = new HashMap<T2, Set<T1>>();
    }

    @Override
    public void add(T1 key, T2 value) {
        Set<T2> l2 = this.directMap.get(key);
        if (l2 == null) {
            l2 = new HashSet<>();
        }
        l2.add((T2) value);
        this.directMap.put((T1) key, l2);

        Set<T1> l1 = this.reversedMap.get(value);
        if (l1 == null) {
            l1 = new HashSet<>();
        }
        l1.add((T1) key);
        this.reversedMap.put((T2) value, l1);
        updateCache(key, l2);
    }

    public void updateCache(T1 key, Set<T2> values) {
        int valueCount = values.size();

        Set<T1> keyByCount = getValuesByCount(valueCount);
        keyByCount.add(key);

        if (valueCount > 1) {
            cache.get(valueCount - 1).remove(key);
            if(cache.get(valueCount-1).size()==0){
                cache.remove(valueCount-1);
            }
        }
    }

    private Set<T1> getValuesByCount(int valuesCount) {
        Set<T1> keysByCount = cache.get(valuesCount);
        if (keysByCount == null) {
            synchronized (cache) {
                keysByCount = cache.get(valuesCount);
                if (keysByCount == null) {
                    keysByCount = Collections.newSetFromMap(new ConcurrentHashMap<>());
                    cache.put(valuesCount, keysByCount);
                }
            }
        }
        return keysByCount;
    }


    @Override
    public Set<T2> get(T1 key) {
        return this.directMap.get(key);
    }

    @Override
    public Set<T1> reversedGet(T2 value) {
        return this.reversedMap.get(value);
    }

    @Override
    public Map<T1, Set<T2>> allObjects() {
        return this.directMap;
    }


    public void clear(){
        directMap.clear();
        reversedMap.clear();
        super.cache.clear();
    }


}
