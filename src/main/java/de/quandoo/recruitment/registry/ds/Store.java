package de.quandoo.recruitment.registry.ds;

import java.util.*;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * represents a class to maintain key-value pairs in the memory.
 * this abstract representation allows us to use further open-source and third-party solutions in the future, 
 * like distributed map. 
 */
public abstract class Store<T1 extends MapKey, T2 extends MapValue> {

	TreeMap<Integer, Set<T1>> cache  = new TreeMap<>((o1, o2) -> -1 * o1.compareTo(o2));


	/**
	 * add a pair of key, value to the store.
	 * with each key there could be a list of values mapped by.
	 * @param key
	 * @param value
	 */
	public abstract void add(T1 key, T2 value);
	
	/**
	 * get the list of values which are mapped by the key
	 * @param key
	 */
	public abstract Set<T2> get(T1 key);
	
	/**
	 * get the list of keys which are mapped to the value
	 * @param value
	 */
	public abstract Set<T1> reversedGet(T2 value);
	
	/**
	 * get all <key,List<value>> pairs stored in the model
	 */
	public abstract Map<T1, Set<T2>> allObjects();
	
	
	/**
	 * sort keys by their length of values and return sorted keys
	 * this is the default sort implementation that uses allObjects() function
	 * and it could be overridden by sub-classes
	 * @param asc
	 */
	public List<T1> sort(boolean asc){
		final int MUL = (asc ? -1 : 1);
		
		final Map<T1, Set<T2>> all = this.allObjects();
		
		final List<T1> keys = new ArrayList<T1> (all.keySet());
		
		final Map<T1, Integer> sizes = new HashMap<T1, Integer>();
		
		all.entrySet().forEach(new Consumer<Map.Entry<T1, Set<T2>>>() {
			Set<T2> value;
			@Override
			public void accept(Entry<T1, Set<T2>> e) {
				value = e.getValue();
				sizes.put(e.getKey(), value == null ? 0 : value.size());	
			}
		});
		
		keys.sort(new Comparator<T1>() {
			@Override
			public int compare(T1 k1, T1 k2) {
				return MUL * sizes.get(k1).compareTo(sizes.get(k2));
			}
		});
		
		return keys;
	}
	
	/**
	 * to select the n first items in sorted keys by their size
	 * @param n
	 */
	public List<T1> topKeys(int n){
		if(n<1) {
			return null;
		}
		final List<T1> keys = this.sort(false);
		return keys.subList(0, Math.min(n, keys.size()));

	}
	public List<T1> topKeysFromCache(int n){
		Stream<T1> resultStream = Stream.empty();
		int counter = n;
		

		for (Iterator<Map.Entry<Integer, Set<T1>>> iterator = cache.entrySet().iterator(); iterator.hasNext(); ) {
		    Entry<Integer, Set<T1>> entry = iterator.next();

            Set<T1> tmpStream = entry.getValue();
			resultStream = Stream.concat(resultStream, entry.getValue().stream());
			if ((counter -= tmpStream.size()) <= 0) {
				break;
			}
		}
		return resultStream.distinct().limit(n).collect(Collectors.toList());
	}
}
