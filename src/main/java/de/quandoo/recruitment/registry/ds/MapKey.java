package de.quandoo.recruitment.registry.ds;

/**
 * represents keys in key-value hash map.
 * each key should have a hashCode function to get an integer from it and an equals method.
 * the two methods is already defined in Object class, but we want to prevent any class to get stored
 * except those which are allowed and also to force Key classes to implement 
 * these 2 methods by their own properties.
 */
public interface MapKey {
	
	public int hashCode();
	
	public boolean equals(Object obj);
		
}
