package de.quandoo.recruitment.registry.ds;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * an extension to DefaultStore to allow concurrent modification of objects
 */
public class ThreadSafeDefaultStore<T1 extends MapKey, T2 extends MapValue> 
	extends DefaultStore<T1, T2>{
	
	public ThreadSafeDefaultStore() {
		this.directMap = new ConcurrentHashMap<T1, Set<T2>>();
		this.reversedMap = new ConcurrentHashMap<T2,Set<T1>>();
	}
	
}
