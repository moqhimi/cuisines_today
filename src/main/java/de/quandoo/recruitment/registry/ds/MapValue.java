package de.quandoo.recruitment.registry.ds;

/**
 * represents values in key-value hash map.
 * it has two methods of hashCode and equals to force value class to implement it, because it is used in 
 * reversedMap and there is queries to search by value.
 * the main aim to have this class is due to prevent any object to get stored except those which are specified.
 */
public interface MapValue {
	
	public int hashCode();
	
	public boolean equals(Object obj);
	
}
